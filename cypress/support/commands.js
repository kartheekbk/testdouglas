Cypress.Commands.add('acceptCookie', () => {
    cy.get('.button__primary').click()
})

Cypress.Commands.add('clickOnUserProfileIcon', () => {
    cy.get('.header-component__button > .button > .icon').click()
})

Cypress.Commands.add('loginWithEmailAndPassword', (username,password) => {
    cy.get('.login__email > .input__inner-wrapper > .input__input').type(username)
    cy.get('.login__password > .input__inner-wrapper > .input__input').type(password)
    cy.get('#loginForm > .button__primary').click()
})

Cypress.Commands.add('checkForInvalidLoginError', () => {
    cy.get('.alert').should('contain.text','Falsche Zugangsdaten')
})

Cypress.Commands.add('loginToDouglas', () => {
    cy.clickOnUserProfileIcon()
    cy.loginWithEmailAndPassword("kartheek90@gmail.com","Test@12345")
    Cypress.$('.button button__with-icon--transparent.account-flyout__button--main').hover()
    //cy.wait(10000);
})

Cypress.Commands.add('verifyIfUserIsLoggedIn', () => {
    cy.get('.account-flyout__status.icon.icon--SVG_19.icon--color-success').should('be.visible');
})

Cypress.Commands.add('logOut', () => {
    cy.get('.header-component__item--account').trigger('mouseenter');
	cy.contains('Abmelden').trigger('click');
})

Cypress.Commands.add('checkIfUserIsLoggedOff', () => {
    cy.get('.icon.icon--SVG_19').should('be.visible');
})


Cypress.Commands.add('clickOnForgotPassword', () => {
    cy.get('.login__link').click()
})

Cypress.Commands.add('enterEmailID', (username) => {
    cy.get('#forgotPasswordForm > .input__container > .input__inner-wrapper > .input__input').type(username)
    cy.get('#forgotPasswordForm > .row > :nth-child(2) > .button').click()
})

Cypress.Commands.add('checkIfMailisTriggered', () => {
    cy.get('.headline-thin').should('contain.text','E-Mail verschickt')
})

